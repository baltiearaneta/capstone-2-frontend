let params = new URLSearchParams(window.location.search)

let token = localStorage.getItem("token")
let adminUser = localStorage.getItem("isAdmin")
console.log(adminUser)

console.log(params.has("courseId"))

let courseId = params.get("courseId")
console.log(courseId)

let courseName = document.querySelector("#courseName")
let courseDesc = document.querySelector("#courseDesc")
let coursePrice = document.querySelector("#coursePrice")
let enrollContainer = document.querySelector("#enrollContainer")

// fetch course info
fetch(`http://localhost:4000/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {
    console.log(data)

        courseName.innerHTML = data.name
        courseDesc.innerHTML = data.description
        coursePrice.innerHTML = data.price

        if (adminUser === "true") {
            enrollContainer.innerHTML = 
                `
                    <table class="table table-striped table-dark my-5">
                        <thead class="">
                            <tr>
                                <td>Enrollees</td>
                                <td>Enrolled On</td>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                `
            
            let enrollTableBody = enrollContainer.querySelector("table tbody")
            console.log(enrollTableBody)
            
                for (enrollee of data.enrollees) {
                let enrolledOn = enrollee.enrolledOn
                console.log(enrolledOn)
                let userId = enrollee.userId
                
                // new route for an admin getting a user IDs details
                fetch(`http://localhost:4000/api/users/admin/details/${userId}`, {
                    method: "GET",
                    headers: {
                        "Authorization": `Bearer ${token}`
                    }
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data)

                    let row = document.createElement("tr")
                    let name = document.createElement("td")
                    name.innerText = `${data.firstName} ${data.lastName}`
                    row.appendChild(name)
                    let enrolledOnNode = document.createElement("td")
                    enrolledOnNode.innerText = enrolledOn 
                    row.appendChild(enrolledOnNode)
                    enrollTableBody.appendChild(row)
                })
            }
        } else {
            // if not an admin user, do the normal thing
            enrollContainer.innerHTML = 
                `
                    <button id="enrollButton" class="btn btn-block btn-primary">
                        Enroll
                    </button>
                `
            let enrollButton = document.querySelector("#enrollButton")
            
            enrollButton.addEventListener("click", () => {
                console.log(token)
                fetch("http://localhost:4000/api/users/enroll", {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": `Bearer ${token}`
                    },
                    body: JSON.stringify({
                        courseId: courseId
                    })
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data)
    
                    if(data === true){
                        alert("Thank you for enrolling, see you in class!")
                        window.location.replace("courses.html")
                    } else {
                        alert("Something went wrong")
                    }
                })
            })
        }
})

    
