let adminUser = localStorage.getItem("isAdmin")
let token = localStorage.getItem("token")
console.log(domain)

// will contain the HTML for the diff. buttons per user
let cardFooter

// note: change the route to the one following REST API convention
fetch(`${domain}/api/courses/`, {
    method: "GET",
    headers: {
        "Authorization": `Bearer ${token}`
    }
})
.then(res => res.json())
.then(data => {
    // console.log(data)

    // variable to store the card, or msg to show if no courses
    let courseData

    if (data.length < 1) {
        courseData = "No courses available."
    } else {
        courseData = data.map(course => {
            console.log(course)

            // logic for rendering diff buttons based on the user
            // if an admin user
            if (adminUser === "true") {
                // relative_path?parameter_name=value
                // course.html?courseId=${course._id}
                if (course.isActive === false) {

                    cardFooter = 
                        `
                            <a href="./course.html?courseId=${course._id}" value={course._id} class="btn btn-info btn-block viewButton">
                                View Course Info
                            </a>
                            <a href="./editCourse.html?courseId=${course._id}" value={course._id} class="btn btn-primary text-white btn-block editButton"> 
                                Edit 
                            </a>
                            <a href="./enableCourse.html?courseId=${course._id}" value={course._id} class="btn btn-success text-white btn-block enableButton"> 
                                Enable Course 
                            </a>
                        `
                } else {

                    cardFooter = 
                        `
                            <a href="./course.html?courseId=${course._id}" value={course._id} class="btn btn-info btn-block viewButton">
                                View Course Info
                            </a>
                            <a href="./editCourse.html?courseId=${course._id}" value={course._id} class="btn btn-primary text-white btn-block editButton"> 
                                Edit 
                            </a>
                            <a href="./deleteCourse.html?courseId=${course._id}" value={course._id} class="btn btn-danger text-white btn-block disableButton"> 
                                Disable Course 
                            </a>
                        `
                }
                



            } else {
                cardFooter =
                    `
                        <a href="./course.html?courseId=${course._id}" value=${course._id} class="btn btn-primary text-white btn-block editButton"> 
                            Select Course 
                        </a>
                    `
            }


            return (
                `
                    <div class="col-md-6 my-3">
                        <div class='card'>
                            <div class='card-body'>
                                <h5 class='card-title'>${course.name}</h5>
                                <p class='card-text text-left'>
                                    ${course.description}
                                </p>
                                <p class='card-text text-right'>
                                    ₱ ${course.price}
                                </p>
                            </div>
                            <div class='card-footer'>
                                ${cardFooter}
                            </div>
                        </div>
                    </div> 
                `
            )
        }).join("")

        let coursesContainer = document.querySelector("#coursesContainer")

        coursesContainer.innerHTML = courseData

    }
})

let modalButton = document.querySelector("#adminButton")

if (adminUser === "false") {
    modalButton.innerHTML = null;
} else {
    modalButton.innerHTML = 
        `
            <div class="col-md-2 offset-md-10">
				<a href="./addCourse.html" class="btn btn-block btn-primary"> Add Course </a>
			</div>
        `
}