let navItems = document.querySelector("#navItems")

let domain = "https://limitless-wave-77208.herokuapp.com"
// let domain = "http://localhost:4000"

// localStorage = object used to store info in our clients
let userToken = localStorage.getItem("token")
console.log(userToken)

// localStorage {
//     getItem: function () => {
//         prints key
//     }
// }

console.log(window.location.pathname)
let pathname = window.location.pathname
console.log(pathname.endsWith("index.html"))
// if there is no user token, ask for log in
if(!userToken) {
    if (pathname === "/capstone-2-frontend/" || pathname.endsWith("index.html")) {
        navItems.innerHTML =
            `      
                <li class="nav-item">
                    <a href="./index.html" class="nav-link">
                        Home
                    </a>
                </li>
                <li class="nav-item">
                    <a href="./pages/login.html" class="nav-link">
                        Login
                    </a>
                </li>
                <li class="nav-item">
                    <a href="./pages/register.html" class="nav-link">
                        Register
                    </a>
                </li>
            `
    } else {
        navItems.innerHTML =
            `      
                <li class="nav-item">
                    <a href="../index.html" class="nav-link">
                        Home
                    </a>
                </li>
                <li class="nav-item">
                    <a href="./login.html" class="nav-link">
                        Login
                    </a>
                </li>
                <li class="nav-item">
                    <a href="./register.html" class="nav-link">
                        Register
                    </a>
                </li>
            `
    }
} else {
    if (pathname.endsWith("index.html")) {
        navItems.innerHTML = 
            `
                <li class="nav-item">
                    <a href="./pages/index.html" class="nav-link">
                        Home
                    </a>
                </li>
                <li class="nav-item">
                    <a href="./pages/courses.html" class="nav-link">
                        Courses
                    </a>
                </li>
                <li class="nav-item">
                    <a href="./pages/profile.html" class="nav-link">
                        Profile
                    </a>
                </li>
                <li class="nav-item">
                    <a href="./pages/logout.html" class="nav-link">
                        Log out
                    </a>
                </li>
            `
    } else {
        navItems.innerHTML = 
            `
                <li class="nav-item">
                    <a href="../index.html" class="nav-link">
                        Home
                    </a>
                </li>
                <li class="nav-item">
                    <a href="courses.html" class="nav-link">
                        Courses
                    </a>
                </li>
                <li class="nav-item">
                    <a href="profile.html" class="nav-link">
                        Profile
                    </a>
                </li>
                <li class="nav-item">
                    <a href="logout.html" class="nav-link">
                        Log out
                    </a>
                </li>
            `
    }
}

