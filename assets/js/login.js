let logInForm = document.querySelector("#logInUser")
console.log(domain)

logInForm.addEventListener("submit", (e) => {

    e.preventDefault()

    let email = logInForm.querySelector("#userEmail").value
    let password = logInForm.querySelector("#password").value
    console.log(email)
    console.log(password)

    if (email == "" || password == "") {
        alert("Please input your email and/or password.")
    }

    fetch(`${domain}/api/users/login`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            email: email,
            password: password 
        })
    })
    .then(res => res.json())
    .then(data => {
        // console.log(data)
        // console.log(data.accessToken)
        
        if (data.accessToken) {
            // store the token in local storage
            localStorage.setItem("token", data.accessToken)
            // localStorage {
            //     token: data.accessToken
            // }
            fetch(`${domain}/api/users/details`, {
                method: "GET",
                headers: {
                    "Authorization": `Bearer ${data.accessToken}`
                }
            })
            .then(res => res.json())
            .then(data => {
                // console.log(data)
                // store the user details in the local storage for future use
                localStorage.setItem("id", data._id)
                localStorage.setItem("isAdmin", data.isAdmin)

                // redirect the user to the courses page
                window.location.replace("courses.html")
            })
        } else {
            alert("Something went wrong.")
        }
    
    
    
    })

})