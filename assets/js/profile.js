let token = localStorage.getItem("token")

fetch(`${domain}/api/users/details`, {
    method: "GET",
    headers: {
        "Authorization": `Bearer ${token}`
    }
})
.then(res => res.json())
.then(data => {
    console.log(data)
    let firstName = data.firstName
    let lastName = data.lastName
    let mobileNo = data.mobileNo
    let email = data.email

    let profileContainer = document.querySelector("#profileContainer")

    profileContainer.innerHTML = 
        `
        <div class="col-md-12">
            <div id="accountInfo" class="row my-4 px-2">
                <div class="col-8">   
                    <div id="firstName" class="row my-1">
                        <b>First Name:</b> 
                        <span></span>
                    </div>
                    <div id="lastName" class="row my-1">
                        <b>Last Name:</b>
                        <span></span>
                    </div>
                    <div id="email" class="row my-1">
                        <b>Email:</b> 
                        <span></span>
                    </div>
                    <div id="mobileNo" class="row my-1">
                        <b>Mobile Number:</b> 
                        <span></span>
                    </div>
                </div>
                <div class="col-4">
                    <div id="editProfile" class="btn btn-success float-right">
                        Edit Profile
                    </div>
                </div>
            </div>
            <div class="row">
                <table id="courseList" class="table table-striped">
                    <thead>
                        <tr>
                            <th>Course Name</th>
                            <th>Description</th>
                            <th>Enrolled On</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
            </div>
        </div>
        `
    
    profileContainer.querySelector("#firstName span").innerText = firstName
    profileContainer.querySelector("#lastName span").innerText = lastName
    profileContainer.querySelector("#email span").innerText = email
    profileContainer.querySelector("#mobileNo span").innerText = mobileNo

    for (row of profileContainer.querySelectorAll("div.row.my-1")) {
        row.querySelector("b").style.width ="125px"

        row.querySelector("span").classList.add("px-2")
    }


    // CODE FOR EDITING PROFILE
    let editProfileBtn = profileContainer.querySelector("#editProfile")
    
    
    const editProfile = function (event) {
        console.log(this)

        profileContainer.querySelector("#firstName span").innerHTML = `<input type="text" placeholder="${firstName}"></input>`
        profileContainer.querySelector("#lastName span").innerHTML = `<input type="text" placeholder="${lastName}"></input>`
        // profileContainer.querySelector("#email span").innerHTML = `<input type="email" placeholder="${email}"></input>`
        profileContainer.querySelector("#mobileNo span").innerHTML = `<input type="number" placeholder="${mobileNo}"></input>`

        console.log("editing profile")

        this.removeEventListener("click", editProfile)
        this.addEventListener("click", saveChanges)
        this.innerText = "Save Changes"


    }

    function getInput (node) {
        if (node.value === "") {
            return node.placeholder
        } else {
            return node.value
        }
    }
    
    const saveChanges = function (event) {
        // Grab the values
        let firstName = getInput(profileContainer.querySelector("#firstName input"))
        let lastName = getInput(profileContainer.querySelector("#lastName input"))
        // let email = getInput(profileContainer.querySelector("#email input"))
        let mobileNo = getInput(profileContainer.querySelector("#mobileNo input"))
        console.log(firstName, lastName, email, mobileNo)
        
        // Update database
        // Check if email is taken
        // console.log(token)
        fetch(`${domain}/api/users/update`, {
            method: "POST",
            headers: {
                "Authorization": `Bearer ${token}`,
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                mobileNo: mobileNo
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            profileContainer.querySelector("#firstName span").innerHTML = `${data.firstName}`
            profileContainer.querySelector("#lastName span").innerHTML = `${data.lastName}`
            profileContainer.querySelector("#mobileNo span").innerHTML = `${data.mobileNo}`
        })


        console.log("saving changes")

        this.removeEventListener("click", saveChanges)
        this.addEventListener("click", editProfile)
        this.innerText = "Edit Profile"

    }
    
    editProfileBtn.addEventListener("click", editProfile)


    // CODE FOR GETTING COURSES
    let courseList = document.querySelector("#courseList")
    
    // get a list of the user's courses
    let courses = data.enrollments
    console.log(courses.length)

    // if no courses, show "no courses to show" in the first row of tbody
    if (courses.length < 1) {
        courseList.querySelector("tbody").innerHTML = 
            `
                <tr>
                    <td colspan="3">No courses enrolled.</td>
                </tr>
            `
    } else {
        // if has courses, iterate through each course
        for (course of courses) {
            let courseId = course.courseId
            fetch(`${domain}/api/courses/${courseId}`)
            .then(res => res.json())
            .then(data => {
                // data = data of ith course
    
                let row = document.createElement("tr")
    
                let name = document.createElement("td")
                name.innerText = data.name
                row.appendChild(name)
    
                let desc = document.createElement("td")
                desc.innerText = data.description
                row.appendChild(desc)
    
                let enrolledOn = document.createElement("td")
                enrolledOn.innerText = course.enrolledOn
                row.appendChild(enrolledOn)
                courseList.querySelector("tbody").appendChild(row)   
            
            })
        }
    }


    
})