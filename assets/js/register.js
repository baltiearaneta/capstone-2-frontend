let registerForm = document.querySelector("#registerUser")

registerForm.addEventListener("submit", (e) => {
    // prevent form from reverting to its default/blank values
    e.preventDefault()

    let firstName = document.querySelector("#firstName").value
    let lastName = document.querySelector("#lastName").value
    let mobileNumber = document.querySelector("#mobileNumber").value
    let userEmail = document.querySelector("#userEmail").value
    let password1 = document.querySelector("#password1").value
    let password2 = document.querySelector("#password2").value

    if ((password1 !== "" && password2 !== "") && (password1 === password2) && mobileNumber.length === 11) {

        // fetch("url", {options})
        // used to process a request (the way we used Postman)
        
        // make sure we dont have any duplicates
        fetch(`${domain}/api/users/email-exists`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: userEmail
            })
        })
        // res.json turns it into a promise
        .then(res => {
            // console.log(res)
            let res2 = res.json()
            // console.log(res2)
            return res2
        })
        .then(data => {
            console.log(data)

            // if email does not exist, register the user
            if (data === false) {
                fetch(`${domain}/api/users/`, {
                    method: "POST",
                    headers: {
                        // the data that I'm passing you is a JSON file
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify({
                        // because we're inside the stringify method, JS knows the left column is the key (which should match the model) and the right column is the actual variable
                        firstName: firstName,
                        lastName: lastName,
                        mobileNo: mobileNumber,
                        email: userEmail,
                        password: password1
                    })
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data)

                    if (data === true) {
                        alert("Registered succesfully")
                        window.location.replace("login.html")
                    } else {
                        alert("Something went wrong.")
                    }
                })
            }
        })

        
    }
 

})

// event {
//     property1: value
// }