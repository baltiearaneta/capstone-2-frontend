// authenticate that the person is an admin
let params = new URLSearchParams(window.location.search)
let token = localStorage.getItem("token")

let courseId = params.get("courseId")
console.log(courseId)

fetch(`${domain}/api/courses/${courseId}/enable`, {
    method: "PUT",
    headers: {
        "Authorization": `Bearer ${token}`
    }
})
.then(res => res.json())
.then(data => {
    console.log(data)

    if(data !== undefined){
        alert("Course enabled")
        window.location.replace("courses.html")
    } else {
        alert("Something went wrong")
    }

})